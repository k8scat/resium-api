# Resource platforms

## [CSDN资源共享规范](https://download.csdn.net/help)

## [百度文库协议](https://wenku.baidu.com/portal/browse/help#help/24)

严禁用户以任何方式转让、出售自己的百度文库账号与积分，一经发现，百度有权立即封禁该账号

## 资源代下网站

* [千文库](http://a.1000wk.com/)
* [免积分](http://www.itziy.com/)
* [QCSDN](http://qcsdn.com/)

* [Catalina 1](http://www.dcsdn.com/)
* [Catalina 2](http://www.catalina.com.cn/)

* [脚本之家电子书下载](https://www.jb51.net/books/)

## 资源网站

* [百度文库VIP](https://wenku.baidu.com/ndvipmember/browse/vipprivilege)
