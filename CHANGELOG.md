[GitLab Changelog](https://gitlab.com/gitlab-org/gitlab/-/blob/master/CHANGELOG.md)

## v6.1.0 (2021-07-11)

- [修改邮箱验证码的邮件模板](./downloader/templates/downloader/email_code.html)
- [修改版权模板](./downloader/templates/downloader/copyright.html)
- [mysqlclient==1.4.4 -> PyMySQL==1.0.2](./docs/mysql.md)

## v6.1.1 (2021-07-22)

- [修复数据库时区问题，调整 Django 数据库的配置](./resium/settings/prod.py)

## v6.1.2 (2021-08-27)

- 适应新版 CSDN 下载
